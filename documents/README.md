
# Funcionalidades
Un usuario envía al sistema las coordenadas gps de una plaza de estacionamiento, que está a punto de dejar libre.
El sistema almacena las coordenadas de la plaza de estacionamiento que ha quedado libre.
El sistema incrementa los puntos del usuario que ha enviado las coordenadas de la plaza de estacionamiento que ha quedado libre.
El usuario usuario que está buscando un plaza de estacionamiento, que tenga la mayor cantidad de puntos es informado de la posición de la plaza de estacionamiento que ha quedado libre.

# Functionalities
A user sends the gps coordinates of a parking space to the system, which he is about to leave free.
The system stores the coordinates of the parking space that has been vacated.
The system increases the points of the user who has sent the coordinates of the free parking space.
The user user who is looking for a parking space, who has the highest number of points is informed of the position of the parking space that has become free.