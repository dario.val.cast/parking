# Parking Project

This is a project developed by Ángel and Darío

## Requirements

In order to build and start the projects, you need to have installed in your system:

* `make`
* `docker`
* `docker-compose`

## deployment

Run backend and data base 

```bash
make docker-start-apps-parking 
```

Tear down the network
```bash
make docker-stop-apps-parking
```

Go inside frontend folder
```bash
cd fronted
```

Generate apk
```bash
ionic cordova run android
```

Run in the browser
```bash
cordova run browser
```