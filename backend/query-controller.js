var db_driver = process.env.DB_DRIVER || 'postgres';

var supported_drivers = require('./supported-drivers'); //TODO include Mongo support

//check db_driver
if (!supported_drivers.includes(db_driver)) {
  console.error('Driver not found! Bye...');
  process.exit(1);
}
if (db_driver === 'postgres') {
  var query = require('./queries/psql/user-queries');
}

module.exports = query;
