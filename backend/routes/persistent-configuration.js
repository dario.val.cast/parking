var express = require('express');
var router = express.Router();
var auth = require('../middleware/check-auth');
var db_query = require('../query-controller');

/* GET ROLES. */
router.get('/roles', async (req, res, next) => {
  try {
    const roles = await db_query.getRoles();
    res.status(200).json({
      status: 'success',
      message: 'Roles fetched successfully',
      data: roles
    });
  } catch (error) {
    throw 'could not fetch Roles';
  }
});

/* GET EVENTS. */
router.get('/events', async (req, res, next) => {
  const events = [
    {
      name: 'Laboratory',
      description: 'lorem ipsum crece de manto',
      icon: 'link'
    },
    {
      name: 'Transport',
      description: 'lorem ipsum crece de manto',
      icon: 'create'
    },
    {
      name: 'Hospital',
      description: 'lorem ipsum crece de manto',
      icon: 'cash'
    }
    // ,
    // {
    //   name: 'Manufacturing',
    //   description: 'lorem ipsum crece de manto',
    //   icon: 'hammer'
    // },
    // {
    //   name: 'Supply',
    //   description: 'lorem ipsum crece de manto',
    //   icon: 'train'
    // },
    // {
    //   name: 'Assembly',
    //   description: 'lorem ipsum crece de manto',
    //   icon: 'build'
    // },
    // {
    //   name: 'Inspection',
    //   description: 'lorem ipsum crece de manto',
    //   icon: 'search'
    // }
  ];

  res.status(200).json({
    status: 'success',
    message: 'Roles fetched successfully',
    data: events
  });
});

module.exports = router;
