var express = require('express');
var router = express.Router();
var bc_query = require('../bc-controller/bc-queries');
var db_query = require('../queries/psql/user-queries');
var auth = require('../middleware/check-auth');
var admin = require('firebase-admin');
var OBJECT_TYPE = 'elementType';

var serviceAccount = require('../bimtrace-firebase-adminsdk-ncy6a-7db0aada5d.json');
var proxyAgent = null;
var proxy = process.env.HTTP_PROXY || '';
if (proxy !== '') {
  tunnel2 = require('tunnel2');
  proxyAgent = tunnel2.httpsOverHttp({
    proxy: {
      host: 'proxy.indra.es',
      port: 8080,
      proxyAuth: 'dcorral:Pbgdctc2000!' // Optional, required only if your proxy require authentication
    }
  });
}

var app = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount, proxyAgent),
  databaseURL: 'https://bimtrace.firebaseio.com'
});

/* GET ELEMENT. */
router.get('/getElement/:elementId', auth, async (req, res, next) => {
  try {
    // Extract elementID from params
    const elementId = req.params.elementId;
    // call BC retrieve element
    const element = await bc_query.getElement(elementId);

    res.status(200).json({
      status: 'success',
      message: 'Element fetched successfully',
      data: element.data
    });
  } catch (error) {
    console.log(error);
    res.status(200).json({
      status: 'error',
      message: 'Element not found',
      data: error
    });
  }
});

/* SENT POSITION. */
router.post('/sendPosition', auth, async (req, res, next) => {
  try {
    // Extract data from body
    const { latitude, longitude, description } = req.body;
    const userId = req.tk_data.email;
    // call BC create element retrieve element
    var element = await bc_query.createElement({
      objectId,
      docType: OBJECT_TYPE,
      name,
      description,
      userId,
      role: String(req.tk_data.role)
    });
    console.log('DOCTYPE', element.docType);
    element.elementId = element.objectId;

    db_query.addElementToUser(userId, objectId);

    res.status(200).json({
      status: 'success',
      message: 'Position sent successfully',
      data: element
    });
  } catch (error) {
    error.message = 'Error sending position';
    next(error);
  }
});

/* UPDATE ELEMENT */
router.post('/updateElement', auth, async (req, res, next) => {
  try {
    // Extract data from body
    let { objectId, eventType, file } = req.body;
    console.log('PRUEBA NOTIFICACION', objectId, eventType);
    if (file === null) {
      file = '';
    }

    var element = {
      objectId,
      docType: OBJECT_TYPE,
      event: {
        role: String(req.tk_data.role),
        eventType: String(eventType),
        userId: req.tk_data.email,
        file
      }
    };

    // call BC create element retrieve element
    element = await bc_query.updateElement(element);
    console.log('UPDATE ELEMENT', element);

    element.eventType = element.docType;

    console.log(element);
    const payload = {
      notification: {
        title: `Element ${objectId} was updated by ${req.tk_data.email}`
      }
    };
    const user = await db_query.getUser(req.tk_data.email);
    console.log('USER', user);
    let tokens = await db_query.getNextPushTokens(element.state).map(token => {
      return token.push_token;
    });
    tokens = tokens.filter(token => {
      return token !== null && token !== undefined;
    });
    console.log('TOKENS:', tokens);
    if (tokens.length > 0) {
      app
        .messaging()
        .sendToDevice(tokens, payload)
        .then(some => {
          console.log(some);
        })
        .catch(error => {
          console.log(error);
        });
    }
    res.status(200).json({
      status: 'success',
      message: 'Element updated successfully',
      data: element
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
});

router.post('/uploadIPFS', auth, async (req, res, next) => {
  try {
    // Extract data from body
    const file = req.files.file;
    var ipfshash = await bc_query.uploadIPFS(file);
    console.log('BACKEND IPFS', ipfshash);

    res.status(200).json({
      status: 'success',
      message: 'Element updated successfully',
      data: ipfshash
    });
  } catch (error) {
    next(error);
  }
});

module.exports = router;
