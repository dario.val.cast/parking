var express = require('express');
var bcrypt = require('bcrypt-nodejs');
var router = express.Router();
const jwt = require('jsonwebtoken');
var db_query = require('../query-controller');
var auth = require('../middleware/check-auth');
const nodemailer = require('nodemailer');
var pgp = require('pg-promise');
var bc_query = require('../bc-controller/bc-queries');
var BD_ERROR_NO_DATA = pgp.errors.queryResultErrorCode.noData;

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'testminsaitbim@gmail.com',
    pass: 'm1ns41tBIM'
  }
});

/* SIGNUP. */
router.post('/signup', async (req, res, next) => {
  try {
    // extract from body
    const { email, password, userRole, firstName, lastName } = req.body;
    // hash password
    let hashed_pass = await bcrypt.hashSync(password);
    // Query DB
    let user = await db_query.createUser(
      email,
      hashed_pass,
      userRole,
      firstName,
      lastName
    );

    // send response back
    res.status(200).json({
      status: 'success',
      message: 'User created successfully',
      data: user
    });
  } catch (error) {
    // throws error to error handler (see error-handler.js)
    next(error);
  }
});

/* LOGIN */
router.post('/login', async (req, res, next) => {
  try {
    // extract from body
    var { email, password } = req.body;
    // Query DB
    var user = await db_query.getUser(email);
    console.log(user);
    // compare hashed password
    if (!bcrypt.compareSync(password, user.password)) {
      throw new Error('Invalid credentials');
    }
    // create JWT
    const token = jwt.sign(
      { email: user.email, role: user.user_role },
      'secret',
      {
        expiresIn: '30 days'
      }
    );

    // send response back
    res.status(200).json({
      status: 'success',
      message: 'User logged in successfully',
      data: {
        token,
        expiresIn: 2592000, // 30 days
        user: {
          id: user.email,
          role: {
            id: user.user_role,
            display_name: user.display_name,
            logo: user.logo
          },
          pub_key: user.pub_key,
          first_name: user.first_name,
          last_name: user.last_name,
          isNew: user.isnew
        }
      }
    });
  } catch (error) {
    if (error.code === BD_ERROR_NO_DATA) {
      error.message =
        'This user is not registered, please contact the Administrator';
    }
    next(error);
  }
});

/* SetIsNewUser. */
router.post('/setIsNewUser', async (req, res, next) => {
  try {
    // extract from body
    const { id } = req.body;
    // Query DB
    await db_query.setIsNewUser(id);

    // send response back
    res.status(200).json({
      status: 'success',
      message: 'User updated successfully',
      data: {}
    });
  } catch (error) {
    // throws error to error handler (see error-handler.js)
    next(error);
  }
});

router.post('/setPushToken', async (req, res, next) => {
  try {
    // extract from body
    const { id, push_token } = req.body;
    // Query DB
    await db_query.setPushToken(id, push_token);

    // send response back
    res.status(200).json({
      status: 'success',
      message: 'User updated successfully',
      data: {}
    });
  } catch (error) {
    // throws error to error handler (see error-handler.js)
    next(error);
  }
});

/* Get User */
router.get('/getUser/:userId', auth, async (req, res, next) => {
  try {
    // Extract userID from params
    const userId = req.params.userId;
    // call BD retrieve user
    const user = await db_query.getUser(userId);

    res.status(200).json({
      status: 'success',
      message: 'User fetched successfully',
      data: {
        email: user.email,
        role: {
          id: user.user_role,
          display_name: user.display_name,
          logo: user.logo
        },
        pub_key: user.pub_key,
        first_name: user.fisrt_name,
        last_name: user.last_name,
        isNew: user.isnew
      }
    });
  } catch (error) {
    next(error);
  }
});

/* CREATE USER VIA INVITATION */
router.post('/createUserInvitation', async (req, res, next) => {
  try {
    // extract from body
    const { email, userRole } = req.body;
    console.log(email);
    const mailOptions = {
      from: 'testminsaitbim@gmail.com', // sender address
      to: email, // list of receivers
      subject: 'PRUEBA MINSAIT BIM', // Subject line
      html: `<h1>WELCOME TO BIM</h1><br><p>This is your invitation to join BIM Trace demo.</p><br><p><b>User:</b> ${email}</p><br><p><b>Password:</b> 1234</b></p>` // plain text body
    };

    let hashed_pass = await bcrypt.hashSync('1234');
    let user = await db_query.createUser(
      email,
      hashed_pass,
      userRole,
      'Mr/s. ' + userRole,
      'Smith'
    );

    transporter.sendMail(mailOptions, function(err, info) {
      if (err) console.log(err);
      else console.log(info);
    });

    // Query DB

    // send response back
    res.status(200).json({
      status: 'success',
      message: 'Invitation sent successfully and user created',
      data: {
        id: user.email,
        role: user.user_role,
        pub_key: user.pub_key,
        first_name: user.first_name,
        last_name: user.last_name,
        isNew: user.isnew
      }
    });
  } catch (error) {
    if (error.code === '23505') {
      error.message = 'User already registered';
    }
    next(error);
    // throws error to error handler (see error-handler.js)
  }
});

/* Get User */
router.get('/getElementsByUser/', auth, async (req, res, next) => {
  try {
    const userId = req.tk_data.email;
    // call BD retrieve user
    const data = await db_query.getElementsByUser(userId);
    console.log(data);
    res.status(200).json({
      status: 'success',
      message: 'Elements fetched successfully',
      data
    });
  } catch (error) {
    next(error);
  }
});

router.post('/addElementToUser', auth, async (req, res, next) => {
  try {
    // extract from body
    const { elementId } = req.body;
    const userId = req.tk_data.email;
    // Query DB
    if (!(await bc_query.isRegistered(elementId))) {
      res.status(200).json({
        status: 'warning',
        message: `Element ${elementId} does not exist on the blockchain`,
        data: {}
      });
    }

    db_query.addElementToUser(userId, elementId);
    // send response back
    res.status(200).json({
      status: 'success',
      message: `Element ${elementId} added to user ${userId}`,
      data: {}
    });
  } catch (error) {
    // throws error to error handler (see error-handler.js)
    console.log(error);
    next(error);
  }
});

// function check_authorized(resource_email, req_email) {
//   if (resource_email !== req_email) {
//     err = new Error();
//     err.code = 'notAuthorized';
//     throw err;
//   }
// }
module.exports = router;
