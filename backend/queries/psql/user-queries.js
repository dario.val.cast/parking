const db = require('../../db-connector');

/* USER QUERIES */
function createUser(email, password, role, firstName, lastName) {
  const query =
    'insert into bimtrace.users (email, password, user_role, first_name, last_name) values($1, $2, $3, $4, $5) returning email, user_role, pub_key';
  return db.one(query, [email, password, role, firstName, lastName]);
}

function getUser(email) {
  const query =
    'select * from bimtrace.users join bimtrace.roles on (bimtrace.users.user_role = bimtrace.roles.id) where email=$1 ';
  return db.one(query, email);
}

function getElementsByUser(email) {
  const query = 'select objectid from bimtrace.searched where userid=$1';
  return db.any(query, email);
}

function getRoles() {
  const query = 'select * from bimtrace.roles';
  return db.many(query);
}

function setIsNewUser(email) {
  const query = 'update bimtrace.users set isnew = FALSE where email=$1';
  return db.none(query, email);
}

function setPushToken(email, token) {
  const query = 'update bimtrace.users set push_token = $2 where email=$1';
  return db.none(query, [email, token]);
}

function addElementToUser(userId, elementId) {
  const query =
    'insert into bimtrace.searched(userid, objectid) values ($1, $2);';
  db.none(query, [userId, elementId]);
}

function getAllPushTokens() {
  const query =
    'SELECT push_token FROM bimtrace.users WHERE push_token IS NOT NULL;';
  return db.any(query);
}
function getNextPushTokens(user_role) {
  const query = `SELECT push_token FROM bimtrace.users WHERE push_token IS NOT NULL AND user_role = 0 OR user_role = ${user_role}+1`;
  return db.any(query);
}

module.exports = {
  //USER QUERIES
  createUser,
  getUser,
  setIsNewUser,
  getRoles,
  setPushToken,
  getAllPushTokens,
  getNextPushTokens,
  getElementsByUser,
  addElementToUser
};
