var db_driver = process.env.DB_DRIVER || 'postgres';

var supported_drivers = require('./supported-drivers'); //TODO include Mongo support

//check db_driver
if (!supported_drivers.includes(db_driver)) {
  console.error('Driver not found! Bye...');
  process.exit(1);
}

if (db_driver === 'postgres') {
  var promise = require('bluebird');

  var options = {
    // Initialization Options
    promiseLib: promise
  };
  var pgp = require('pg-promise')(options);

  //DB CONNECTION
  const dbHost = process.env.POSTGRES_HOST || 'localhost';
  const dbPort = process.env.POSTGRESS_PORT || '5432';
  const dbUser = process.env.POSTGRESS_USER || 'postgres';
  const dbName = process.env.POSTGRES_DB || 'postgres';
  const dbPass = process.env.POSTGRES_PASSWORD || 'example';

  try {
    var connectionString = `postgres://${dbUser}:${dbPass}@${dbHost}:${dbPort}/${dbName}`;
    var db = pgp(connectionString);
  } catch (error) {
    console.log(error);
  }
}

module.exports = db;
