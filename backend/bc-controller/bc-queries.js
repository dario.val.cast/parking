const request = require('request-promise');
const API_KEY = 123456;

const protocol = 'http';
const host = process.env.MIDDLEWARE_HOST || 'localhost';
const port = process.env.MIDDLEWARE_PORT || '3000';

const url = `${protocol}://${host}:${port}/api/mediChain`;
const urlIpfs = `${protocol}://${host}:${port}`;
console.info(`Prepared to connect to middleware: ${host}:${port}`);

function isRegistered(elementId) {
  return request({
    method: 'GET',
    uri: `${url}/elementId/${elementId}`,
    headers: { api_key: API_KEY },
    json: true
  }).then(response => {
    return response.code === 0;
  });
}

// GET ELEMENT
function getElement(elementId) {
  return request({
    method: 'GET',
    uri: `${url}/elementId/${elementId}`,
    headers: { api_key: API_KEY },
    json: true
  })
    .then(response => {
      console.log('RESPONSE', response);
      if (response.code === 0) {
        return response;
      } else {
        throw new Error(response.message);
      }
    })
    .catch(err => {
      console.log('CATCH BC', err);
      throw err;
    });
}

// CREATE ELEMENT
function createElement(element) {
  return request({
    method: 'POST',
    uri: `${url}/createObject`,
    headers: { api_key: API_KEY },
    body: element,
    json: true
  })
    .then(response => {
      console.log('RESPONSE', response);
      if (response.code === 0) {
        return response.data;
      } else {
        err = new Error(response.message);
        throw err;
      }
    })
    .catch(err => {
      console.log('CATCH BC', err);
      throw err;
    });
}

// UPDATE ELEMENT
function updateElement(element) {
  console.log('ELEMENT', element);
  return request({
    method: 'POST',
    uri: `${url}/updateObject`,
    headers: { api_key: API_KEY },
    body: element,
    json: true
  })
    .then(response => {
      console.log('RESPONSE', response);
      if (response.code === 0) {
        return response.data;
      } else {
        err = new Error(response.message);
        throw err;
      }
    })
    .catch(err => {
      console.log('CATCH BC', err);
      throw err;
    });
}

function uploadIPFS(file) {
  var options = {
    method: 'POST',
    url: `${urlIpfs}/api/ipfscreatedocument`,
    headers: {
      'cache-control': 'no-cache',
      'Content-Type': 'application/x-www-form-urlencoded',
      api_key: API_KEY
    },
    formData: {
      file: {
        value: file.data,
        options: { filename: file.name, contentType: null }
      },
      fileName: file.name
    },
    json: true
  };
  return request(options)
    .then(response => {
      console.log('RESPONSE FILE BC', response);
      if (response.code === 0) {
        return response.data;
      } else {
        err = new Error(response.data);
        err.code = 'BC_ERR';
        throw err;
      }
    })
    .catch(err => {
      console.log('CATCH BC', err);
      throw err;
    });
}

module.exports = {
  getElement,
  createElement,
  updateElement,
  uploadIPFS,
  isRegistered
};
