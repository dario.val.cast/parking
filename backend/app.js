var errorHandler = require('./errors/error-handler');
(express = require('express')),
  (cors = require('cors')),
  (bodyParser = require('body-parser')),
  (usersRouter = require('./routes/users')),
  (persistentConfigRouter = require('./routes/persistent-configuration')),
  (elementsRouter = require('./routes/elements')),
  (fileUpload = require('express-fileupload')),
  (app = express());
// configuration
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(fileUpload());

// routes
app.use('/users', usersRouter);
app.use('/elements', elementsRouter);
app.use('/persistentConfig', persistentConfigRouter);
//app.use('/car', persistentConfigRouter);
// middlewares
app.use(errorHandler);

module.exports = app;
