import { Component, OnInit, SystemJsNgModuleLoaderConfig } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-legal',
  templateUrl: './legal.page.html',
  styleUrls: ['./legal.page.scss']
})
export class LegalPage implements OnInit {
  constructor(private modalCtrl: ModalController) {}

  ngOnInit() {}

  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true
    });
  }
}
