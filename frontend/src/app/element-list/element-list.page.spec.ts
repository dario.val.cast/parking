import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementListPage } from './element-list.page';

describe('ElementListPage', () => {
  let component: ElementListPage;
  let fixture: ComponentFixture<ElementListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
