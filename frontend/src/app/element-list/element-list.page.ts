import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';
import { LoadingOptions } from '@ionic/core';
import { LoadingController, NavController } from '@ionic/angular';
import { ElementService } from '../services/element.service';

@Component({
  selector: 'app-element-list',
  templateUrl: './element-list.page.html',
  styleUrls: ['./element-list.page.scss']
})
export class ElementListPage implements OnInit {
  elementsSubs: Subscription;
  elementSubs: Subscription;
  elements = [];
  loadingElement: HTMLIonLoadingElement;
  constructor(
    private auhtService: AuthService,
    private loadingCtrl: LoadingController,
    public navCtrl: NavController,
    private elementService: ElementService
  ) {}

  async ngOnInit() {
    this.elementsSubs = this.auhtService
      .getElementsByUserListener()
      .subscribe(elements => {
        this.elements = elements;
        console.log(this.elements);
        this.loadingElement.dismiss();
      });
    this.elementSubs = this.elementService
      .getElementListener()
      .subscribe(element => {
        this.loadingElement.dismiss();
        this.navCtrl.navigateForward('element-status');
      });
    this.loadingElement = await this.loadingCtrl.create({
      keyboardClose: true,
      message: 'Fetching data from the Blockchain'
    });
    this.loadingElement.present();
    this.auhtService.getElementsByUser();
  }

  ionViewWillEnter() {}

  async goToElement(elementId) {
    this.loadingElement = await this.loadingCtrl.create({
      keyboardClose: true,
      message: 'Fetching data from the Blockchain'
    });
    this.loadingElement.present();
    this.elementService.getElement(elementId);
  }

  ionViewDidLeave() {
    // this.elementSubs.unsubscribe();
  }
}
