import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventStepperPage } from './event-stepper.page';

describe('EventStepperPage', () => {
  let component: EventStepperPage;
  let fixture: ComponentFixture<EventStepperPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventStepperPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventStepperPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
