import { Component, OnInit, ViewChild } from '@angular/core';
import {
  NavController,
  AlertController,
  LoadingController,
  IonRefresher
} from '@ionic/angular';
import { ElementService } from '../services/element.service';
import {
  BarcodeScanner,
  BarcodeScannerOptions
} from '@ionic-native/barcode-scanner/ngx';
import { AlertOptions } from '@ionic/core';
import { Subscription } from 'rxjs';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { User } from '../user.model';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-event-stepper',
  templateUrl: './event-stepper.page.html',
  styleUrls: ['./event-stepper.page.scss']
})
export class EventStepperPage implements OnInit {
  public element: any = undefined;
  qrScannerOpts: BarcodeScannerOptions;
  cameraOptions: CameraOptions;
  scannInfo: any;
  getElement: Subscription;
  getIPFS: Subscription;
  loadingElement: HTMLIonLoadingElement = null;
  IPFSHash: string = null;
  canConfirm = true;
  user: User;
  navigate = false;

  constructor(
    public navCtrl: NavController,
    private qrScanner: BarcodeScanner,
    private elementService: ElementService,
    public alertController: AlertController,
    private camera: Camera,
    private loadingCtrl: LoadingController,
    private authservice: AuthService
  ) {
    this.element = this.elementService.getElementLocal();
    this.qrScannerOpts = {
      showTorchButton: true,
      prompt: 'Point your camera to a QR code',
      resultDisplayDuration: 0,
      disableSuccessBeep: false
    };
    this.cameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
  }

  ngOnInit() {
    this.element = this.elementService.getElementLocal();
    console.log('ELEMENT', this.element);
    this.user = this.authservice.getUser();
    this.getIPFS = this.elementService.getIPFSListener().subscribe(response => {
      this.IPFSHash = response;
      this.loadingElement.dismiss();
      this.securityCheck();
    });
    this.getElement = this.elementService.getElementListener().subscribe(
      response => {
        this.element = response.element;
        this.canConfirm = true;
        if (this.navigate) {
          this.loadingElement.dismiss();
          this.navCtrl.navigateForward(
            'element-detail/' + this.element.objectId
          );
          this.navigate = false;
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  async navigateDetail() {
    this.loadingElement = await this.loadingCtrl.create({
      keyboardClose: true,
      message: 'Fetching data from the Blockchain'
    });
    this.loadingElement.present();
    this.navigate = true;
    this.elementService.getElement(this.element.objectId);
  }

  nextState() {
    this.showAlert({
      header: 'Attach document',
      subHeader: 'Do you want to attach a picture to your update?',
      buttons: [
        {
          text: 'No, just update',
          handler: () => {
            this.securityCheck();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.takePicture();
          }
        }
      ]
    });
  }

  takePicture() {
    this.camera.getPicture(this.cameraOptions).then(
      async imageData => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        const base64Image = 'data:image/jpeg;base64,' + imageData;
        this.loadingElement = await this.loadingCtrl.create({
          keyboardClose: true,
          message: 'Attaching picture to event...'
        });
        const picture = this.dataURItoBlob(base64Image);
        this.loadingElement.present();
        this.canConfirm = false;
        this.elementService.uploadIPFS(picture);
      },
      err => {
        // Handle error
      }
    );
  }

  dataURItoBlob(dataURI) {
    let array, binary, i;
    binary = atob(dataURI.split(',')[1]);
    array = [];
    i = 0;
    while (i < binary.length) {
      array.push(binary.charCodeAt(i));
      i++;
    }
    return new Blob([new Uint8Array(array)], {
      type: 'image/jpeg'
    });
  }
  private dataURLtoFile(dataurl, filename) {
    const arr = dataurl.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }

  securityCheck() {
    this.showAlert({
      header: 'Security check',
      subHeader:
        'For security reasons we need you to scan again the QR code of your shipment',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Accept',
          cssClass: 'button-accept',
          handler: () => {
            this.scann();
          }
        }
      ]
    });
  }

  private scann() {
    console.log('SCANN ELEMENT', this.element);
    this.qrScanner
      .scan(this.qrScannerOpts)
      .then(result => {
        if (result.cancelled || result.text.includes('http')) {
          const AlertOpts: AlertOptions = {
            header: 'Invalid QR',
            subHeader:
              'This QR is not valid, please try again with a different one.',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              },
              {
                text: 'Try again',
                cssClass: 'button-warn',
                handler: () => {
                  this.scann();
                }
              }
            ]
          };
          this.showAlert(AlertOpts);
          return;
        }

        this.scannInfo = result.text;
        if (this.scannInfo === this.element.objectId) {
          // update here
          this.canConfirm = false;
          this.elementService.updateElement({
            objectId: this.scannInfo,
            eventType: String(this.element.state),
            file: this.IPFSHash
          });
        } else {
          alert('ups! nope!');
          return;
        }
      })
      .catch(error => {
        const AlertOpts: AlertOptions = {
          header: 'Invalid QR',
          subHeader:
            'This QR is not valid, please try again with a different one.',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel'
            },
            {
              text: 'Try again',
              cssClass: 'button-warn',
              handler: () => {
                this.nextState();
              }
            }
          ]
        };
        this.showAlert(AlertOpts);
      });
  }

  async showAlert(alertOpts: AlertOptions) {
    const alert = await this.alertController.create(alertOpts);

    await alert.present();
  }
}
