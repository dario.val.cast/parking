import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EventStepperPage } from './event-stepper.page';
import { StepperComponent } from '../stepper/stepper.component';

const routes: Routes = [
  {
    path: '',
    component: EventStepperPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EventStepperPage, StepperComponent]
})
export class EventStepperPageModule {}
