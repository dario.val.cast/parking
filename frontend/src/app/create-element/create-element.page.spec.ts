import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateElementPage } from './create-element.page';

describe('CreateElementPage', () => {
  let component: CreateElementPage;
  let fixture: ComponentFixture<CreateElementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateElementPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateElementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
