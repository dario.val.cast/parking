import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ElementService } from '../services/element.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {
  AlertController,
  NavController,
  LoadingController
} from '@ionic/angular';
import { element } from 'protractor';
import { AlertOptions, LoadingOptions, ViewController } from '@ionic/core';

@Component({
  selector: 'app-create-element',
  templateUrl: './create-element.page.html',
  styleUrls: ['./create-element.page.scss']
})
export class CreateElementPage implements OnInit {
  objectId = '';
  elementSub: Subscription;
  alertOpts: AlertOptions;
  loadingOpts: LoadingOptions;
  loadingElement: HTMLIonLoadingElement;
  constructor(
    private elementService: ElementService,
    private route: ActivatedRoute,
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private loadingCtrl: LoadingController
  ) {
    this.objectId = this.route.snapshot.paramMap.get('objectId');
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.elementSub = this.elementService.getElementListener().subscribe(
      response => {
        if (response.element) {
          const elementObj = response.element;
          this.alertOpts = {
            header: 'Shipment created!',
            subHeader:
              'Shipment: ' +
              elementObj.objectId +
              ' was successfully registered in the blockchain',
            buttons: [
              {
                text: 'Nice! :)',
                handler: () => {
                  this.navCtrl.navigateRoot('element-status');
                }
              }
            ]
          };

          this.showAlert(this.alertOpts);
        }
        this.loadingElement.dismiss();
      },
      error => {
        console.group(error);
        this.loadingElement.dismiss();
      }
    );
  }

  async onSubmit(form: NgForm) {
    const name = form.value.name;
    const description = form.value.description;
    this.loadingElement = await this.loadingCtrl.create({
      keyboardClose: true,
      message: 'Registering shipment on Blockchain, please wait'
    });
    this.loadingElement.present();
    this.elementService.createElement({
      objectId: this.objectId,
      name,
      description
    });
  }

  async showAlert(alertOpts: AlertOptions) {
    const alert = await this.alertCtrl.create(alertOpts);
    await alert.present();
  }

  ionViewDidLeave() {
    this.elementSub.unsubscribe();
  }
}
