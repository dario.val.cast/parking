import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { AuthService } from '../auth/auth.service';
import { User } from '../user.model';
import { AlertOptions } from '@ionic/core';
import { InspectPage } from '../inspect/inspect.page';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss']
})
export class DashboardPage implements OnInit {
  user: User;
  constructor(
    public navCtrl: NavController,
    private authService: AuthService,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  logout() {
    const alertopts: AlertOptions = {
      header: 'Confirm',
      message: 'Do you really want to log out?',
      buttons: [
        {
          text: 'Yes',
          cssClass: 'button-accept',
          handler: () => {
            this.authService.logout();
          }
        },
        {
          text: 'No',
          role: 'cancel'
        }
      ]
    };
    this.presentAlertConfirm(alertopts);
  }
  async presentAlertConfirm(alertOpts: AlertOptions) {
    const alert = await this.alertCtrl.create(alertOpts);

    await alert.present();
  }
}
