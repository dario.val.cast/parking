import { Component, OnInit } from '@angular/core';

import {
  Platform,
  AlertController,
  NavController,
  ToastController
} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './auth/auth.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { AlertOptions } from '@ionic/core';
import { FCM } from '@ionic-native/fcm/ngx';
import { ModalController } from '@ionic/angular';
import { LegalPage } from './legal/legal.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private screenOrientation: ScreenOrientation,
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    public toastController: ToastController,
    private fcm: FCM,
    private modalCtrl: ModalController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(true);
      this.splashScreen.hide();
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.fcm.onNotification().subscribe(notification => {
        console.log(notification);
        let toast: HTMLIonToastElement = null;
        this.toastController
          .create({
            message: notification.title,
            showCloseButton: true,
            duration: 5000,
            position: 'middle'
          })
          .then(newToast => {
            toast = newToast;
            toast.present();
          });
      });
      this.presentModal();
    });
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: LegalPage
    });
    return await modal.present();
  }
  onNavDashboard() {
    this.navCtrl.navigateRoot('dashboard');
  }
  onNavElementList() {
    this.navCtrl.navigateRoot('element-list');
  }

  onNavProfile() {
    this.navCtrl.navigateForward('profile');
  }

  ngOnInit() {
    this.authService.autoAuthUser();
  }

  onLogout() {
    const alertopts: AlertOptions = {
      header: 'Confirm',
      message: 'Do you really want to log out?',
      buttons: [
        {
          text: 'Yes',
          cssClass: 'button-accept',
          handler: () => {
            this.authService.logout();
          }
        },
        {
          text: 'No',
          role: 'cancel'
        }
      ]
    };
    this.presentAlertConfirm(alertopts);
  }

  async presentAlertConfirm(alertOpts: AlertOptions) {
    const alert = await this.alertCtrl.create(alertOpts);

    await alert.present();
  }
}
