import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { AuthService } from './auth.service';
import { LoadingController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { PersistentConfigService } from '../services/persistent-config.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss']
})
export class AuthPage implements OnInit {
  isLogin = true;
  private authStatusSub: Subscription;
  showPass = false;
  loaingElement: HTMLIonLoadingElement = null;

  constructor(
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private persistentConfigService: PersistentConfigService
  ) {}

  ngOnInit() {
    this.authStatusSub = this.authService.getAuthStatusListener().subscribe(
      authStatus => {
        if (this.loaingElement) {
          this.loaingElement.dismiss();
        }
      },
      error => {
        this.loaingElement.dismiss();
      }
    );
  }

  togglePass() {
    this.showPass = !this.showPass;
  }

  async onAuthenticate(user: any) {
    let message = 'Login in...';
    if (!this.isLogin) {
      this.authService.signup(user);
      message = 'Signing up...';
    } else {
      this.authService.login(user);
    }
    this.loaingElement = await this.loadingCtrl.create({
      keyboardClose: true,
      message
    });
    this.loaingElement.present();
  }

  onSwitchAuthMode(form: NgForm) {
    form.reset();
    this.isLogin = !this.isLogin;
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const password = form.value.password;

    if (this.isLogin) {
      // login
      form.reset();
      this.onAuthenticate({ email, password });
    } else {
      // signup
      const userRole = form.value.category;
      const firstName = form.value.firstName;
      const lastName = form.value.lastName;

      form.reset();
      this.onAuthenticate({ email, password, userRole, firstName, lastName });
    }
  }
}
