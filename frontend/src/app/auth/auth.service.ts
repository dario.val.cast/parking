import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Response } from '../response-model';
import { Storage } from '@ionic/storage';
import { NavController, AlertController } from '@ionic/angular';
import { User } from '../user.model';
import { AlertOptions } from '@ionic/core';
import { PersistentConfigService } from '../services/persistent-config.service';
import { FCM } from '@ionic-native/fcm/ngx';
import { element } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isAuthenticated = false;
  private token: string;
  private user: User;
  private authStatusListener = new Subject<any>();
  private elementsListener = new Subject<any>();
  private tokenTimer: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    private storage: Storage,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private persistentConfigService: PersistentConfigService,
    private fcm: FCM
  ) {}

  // Getters
  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }
  getElementsByUserListener() {
    return this.elementsListener.asObservable();
  }

  getUserIsAuthenticated() {
    return this.isAuthenticated;
  }

  getToken() {
    return this.token;
  }

  getUser() {
    return this.user;
  }

  // Endpoints
  signup(user: {
    email: string;
    password: string;
    userRole: string;
    firstName: string;
    lastName: string;
  }) {
    this.http
      .post<Response>(environment.apiUrl + '/users/signup', user)
      .subscribe(
        _ => {
          this.login({ email: user.email, password: user.password });
        },
        error => {
          console.log(error);
          this.authStatusListener.next(false);
        }
      );
  }

  login(user: { email: string; password: string }) {
    this.http
      .post<Response>(environment.apiUrl + '/users/login', user)
      .subscribe(
        res => {
          try {
            this.token = res.data.token;
            console.log('TOKEN', this.token);
            if (this.token) {
              this.persistentConfigService.getEvents();
              this.persistentConfigService.getRoles();
              const expiresIn = res.data.expiresIn;
              // this.setAuthTimer(expiresIn);
              this.isAuthenticated = true;
              this.user = res.data.user;
              this.authStatusListener.next(true);
              const now = new Date();
              const expiration = new Date(now.getTime() + expiresIn * 1000);
              this.saveAuthData(this.token, expiration, this.user);
              if (this.user.isNew) {
                this.navCtrl.navigateRoot('/welcome');
              } else {
                this.navCtrl.navigateRoot('/dashboard');
              }
              this.initFCM();
            }
          } catch (error) {
            console.log(error);
          }
        },
        error => {
          console.log(error);
          this.logout();
          this.authStatusListener.next(false);
        }
      );
  }

  setIsNewUser() {
    this.http
      .post<Response>(environment.apiUrl + '/users/setIsNewUser', {
        id: this.user.id
      })
      .subscribe(
        response => {
          this.user.isNew = false;
        },
        error => {
          console.log(error);
        }
      );
  }

  addElementToUser(elementId: string) {
    this.http
      .post<Response>(environment.apiUrl + '/users/addElementToUser', {
        elementId
      })
      .subscribe(
        response => {
          console.log(response);
        },
        error => {
          console.log(error);
        }
      );
  }

  getElementsByUser(): any {
    this.http
      .get<Response>(environment.apiUrl + '/users/getElementsByUser')
      .subscribe(
        response => {
          this.elementsListener.next(response.data);
        },
        error => {
          console.log(error);
        }
      );
  }

  setPushToken(token) {
    this.http
      .post<Response>(environment.apiUrl + '/users/setPushToken', {
        id: this.user.id,
        push_token: token
      })
      .subscribe(
        response => {
          this.user.isNew = false;
        },
        error => {
          console.log(error);
        }
      );
  }

  async logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    // clearTimeout(this.tokenTimer);
    await this.clearAuthData();
    this.navCtrl.navigateRoot('/auth');
  }

  createUserInvitation(user: { email: string; userRole: string }) {
    this.http
      .post<Response>(environment.apiUrl + '/users/createUserInvitation', user)
      .subscribe(
        response => {
          const alertopts: AlertOptions = {
            header: 'Invitation sent',
            message: response.message,
            buttons: [
              {
                text: 'Nice!',
                handler: () => {
                  this.navCtrl.navigateRoot('');
                }
              }
            ]
          };
          this.presentAlertConfirm(alertopts);
        },
        error => {
          console.log(error);
        }
      );
  }

  async autoAuthUser() {
    const authInfo = await this.getAuthData();
    if (!authInfo) {
      return;
    }
    const now = new Date();
    const expiresIn = authInfo.expiration.getTime() - now.getTime();
    if (expiresIn > 0) {
      this.token = authInfo.token;
      this.user = authInfo.user;
      this.isAuthenticated = true;
      this.navCtrl.navigateRoot('/dashboard');
      // this.setAuthTimer(expiresIn / 1000);
      this.persistentConfigService.getEvents();
      this.persistentConfigService.getRoles();
      this.authStatusListener.next(true);
      this.initFCM();
    }
  }

  // UTILS
  private async getAuthData() {
    const token = await this.storage.get('token');
    const expiration = await this.storage.get('expiration');
    const user = await this.storage.get('user');
    return {
      token,
      expiration: new Date(expiration),
      user
    };
  }

  private saveAuthData(token: string, expirationDate: Date, user: User) {
    this.storage.set('token', token);
    this.storage.set('expiration', expirationDate.toISOString());
    this.storage.set('user', user);
  }

  private async clearAuthData() {
    await this.storage.clear();
  }

  private setAuthTimer(duration: any) {
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

  async presentAlertConfirm(alertOpts: AlertOptions) {
    const alert = await this.alertCtrl.create(alertOpts);

    await alert.present();
  }

  initFCM() {
    this.fcm.getToken().then(token => {
      this.setPushToken(token);
    });
  }
}
