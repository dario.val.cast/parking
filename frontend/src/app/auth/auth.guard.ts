import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(
    private authService: AuthService,
    private navCtrl: NavController
  ) {}
  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {
    const isAuth = this.authService.getUserIsAuthenticated();
    const user = this.authService.getUser();
    if (!isAuth) {
      this.navCtrl.navigateRoot('/auth');
    }
    if (route.path === '/create-user' && user.role.id !== 0) {
      this.navCtrl.navigateRoot('');
    }
    return isAuth;
  }
}
