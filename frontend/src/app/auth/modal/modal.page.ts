import { Component, Input } from '@angular/core';
import { User } from 'src/app/user.model';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss']
})
export class ModalPage {
  @Input() user: User;
  constructor() {}
}
