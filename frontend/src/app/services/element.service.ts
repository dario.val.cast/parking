import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Response } from '../response-model';

@Injectable({
  providedIn: 'root'
})
export class ElementService {
  private elementGetListener = new Subject<any>();
  private IPFSGetListener = new Subject<any>();
  private element: any;

  constructor(private http: HttpClient) {}

  // getElementlocal caching
  getElementLocal() {
    return this.element;
  }

  setElementLocal(element) {
    this.element = element;
  }

  // Getters
  getElementListener() {
    return this.elementGetListener.asObservable();
  }
  getIPFSListener() {
    return this.IPFSGetListener.asObservable();
  }

  // Endpoints
  getElement(objectId: string) {
    this.http
      .get<Response>(environment.apiUrl + '/elements/getElement/' + objectId)
      .subscribe(response => {
        if (response.status === 'error') {
          this.elementGetListener.next({ exists: false });
        } else {
          this.element = response.data;
          this.elementGetListener.next({
            exists: true,
            element: response.data
          });
        }
      });
  }

  createElement(element: {
    objectId: string;
    name: string;
    description: string;
  }) {
    this.http
      .post<Response>(environment.apiUrl + '/elements/createElement', element)
      .subscribe(
        response => {
          this.element = response.data;
          this.elementGetListener.next({ element: response.data });
        },
        error => {
          this.elementGetListener.next({ error: error.data });
        }
      );
  }

  updateElement(element: {
    objectId: string;
    eventType: string;
    file: string;
  }) {
    console.log(element);
    this.http
      .post<Response>(environment.apiUrl + '/elements/updateElement', element)
      .subscribe(
        response => {
          this.element = response.data;
          this.elementGetListener.next({ element: response.data });
        },
        error => {
          this.elementGetListener.next({ error: error.data });
        }
      );
  }

  uploadIPFS(file: Blob) {
    const documentData = new FormData();
    documentData.append('file', file, +new Date().valueOf() + '');
    this.http
      .post<Response>(environment.apiUrl + '/elements/uploadIPFS', documentData)
      .subscribe(
        response => {
          this.IPFSGetListener.next(response.data);
        },
        error => {}
      );
  }
}
