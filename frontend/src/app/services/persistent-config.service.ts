import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Response } from '../response-model';

@Injectable({
  providedIn: 'root'
})
export class PersistentConfigService {
  private rolesGetListener = new Subject<any>();
  private roles: any;
  private eventsGetListener = new Subject<any>();
  private events: any;

  constructor(private http: HttpClient) {}

  // getRoleslocal caching
  getRolesLocal() {
    return this.roles;
  }
  // getEventslocal caching
  getEventsLocal() {
    return this.events;
  }

  // Getters
  getRolesListener() {
    return this.rolesGetListener.asObservable();
  }

  getEventsListener() {
    return this.eventsGetListener.asObservable();
  }

  // Endpoints
  getRoles() {
    this.http
      .get<Response>(environment.apiUrl + '/persistentConfig/roles')
      .subscribe(
        response => {
          this.roles = response.data;
          this.rolesGetListener.next({
            roles: response.data
          });
        },
        error => {
          this.rolesGetListener.next(false);
        }
      );
  }

  getEvents() {
    this.http
      .get<Response>(environment.apiUrl + '/persistentConfig/events')
      .subscribe(
        response => {
          this.events = response.data;
          this.eventsGetListener.next({
            events: response.data
          });
        },
        error => {
          this.eventsGetListener.next(false);
        }
      );
  }
}
