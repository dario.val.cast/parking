export interface Role {
  id: number;
  display_name: string;
  logo: string;
}
