import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectPage } from './inspect.page';

describe('InspectPage', () => {
  let component: InspectPage;
  let fixture: ComponentFixture<InspectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
