import { Component, OnInit } from '@angular/core';
import {
  BarcodeScanner,
  BarcodeScannerOptions
} from '@ionic-native/barcode-scanner/ngx';
import { ElementService } from '../services/element.service';
import { Subscription } from 'rxjs';
import {
  AlertController,
  NavController,
  LoadingController
} from '@ionic/angular';
import { AlertOptions } from '@ionic/core';
import { User } from '../user.model';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-inspect',
  templateUrl: './inspect.page.html',
  styleUrls: ['./inspect.page.scss']
})
export class InspectPage implements OnInit {
  scannInfo: any;
  qrScannerOpts: BarcodeScannerOptions;
  getElement: Subscription;
  loadingElement: HTMLIonLoadingElement = null;
  user: User = null;
  logoUrl = 'assets/qr-code.png';
  constructor(
    private qrScanner: BarcodeScanner,
    private navCtrl: NavController,
    private elementService: ElementService,
    public alertController: AlertController,
    private loadingCtrl: LoadingController,
    private authService: AuthService
  ) {
    this.qrScannerOpts = {
      showTorchButton: true,
      prompt: 'Point your camera to a QR code',
      resultDisplayDuration: 0,
      disableSuccessBeep: false
    };
  }

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  ionViewWillEnter() {
    this.user = this.authService.getUser();
    this.getElement = this.elementService.getElementListener().subscribe(
      response => {
        this.loadingElement.dismiss();
        if (response.element) {
          this.scannInfo = response.element;
          this.navCtrl.navigateForward('element-status');
        } else {
          if (this.user.role.id === 0) {
            const AlertOpts: AlertOptions = {
              header: 'This shipment is not on the Blockchain',
              message: 'Do you want to create a new record for this shipment?',
              buttons: [
                {
                  text: 'No',
                  role: 'cancel'
                },
                {
                  text: 'Yes',
                  cssClass: 'button-accept',
                  handler: () => {
                    this.navCtrl.navigateForward(
                      'create-element/' + this.scannInfo
                    );
                  }
                }
              ]
            };
            this.showAlert(AlertOpts);
          } else {
            const AlertOpts: AlertOptions = {
              header: 'Element not found',
              message:
                'Sorry, this element is not registered on the blockchain. Contact your BIM Manager to solve this issue',
              buttons: [
                {
                  text: 'Ok',
                  role: 'cancel',
                  handler: () => {
                    this.navCtrl.navigateRoot('');
                  }
                }
              ]
            };
            this.showAlert(AlertOpts);
          }
        }
      },
      error => {
        this.loadingElement.dismiss();
        const AlertOpts: AlertOptions = {
          header: 'Unspected error',
          subHeader: error,
          buttons: [
            {
              text: 'Back',
              handler: () => {
                this.navCtrl.navigateRoot('');
              }
            }
          ]
        };
        this.showAlert(AlertOpts);
      }
    );
    if (!this.user.isNew) {
      this.scann();
    }
  }

  scann() {
    if (this.user.isNew) {
      this.authService.setIsNewUser();
    }
    this.qrScanner
      .scan(this.qrScannerOpts)
      .then(async result => {
        if (result.cancelled) {
          return;
        }
        if (result.text.includes('http')) {
          const AlertOpts: AlertOptions = {
            header: 'Invalid QR',
            subHeader:
              'This QR is not valid, please try again with a different one.',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel'
              },
              {
                text: 'Try again',
                cssClass: 'button-warn',
                handler: () => {
                  this.scann();
                }
              }
            ]
          };
          this.showAlert(AlertOpts);
          return;
        }
        this.scannInfo = result.text.trim();
        this.loadingElement = await this.loadingCtrl.create({
          keyboardClose: true,
          message: 'Fetching data from Blockchain...'
        });
        this.loadingElement.present();
        this.elementService.getElement(this.scannInfo);
        this.authService.addElementToUser(this.scannInfo);
      })
      .catch(error => {});
  }

  ionViewDidLeave() {
    this.getElement.unsubscribe();
  }

  async showAlert(alertOpts: AlertOptions) {
    const alert = await this.alertController.create(alertOpts);

    await alert.present();
  }
}
