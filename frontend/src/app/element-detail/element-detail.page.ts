import { Component, OnInit, ViewChild } from '@angular/core';
import { ElementService } from '../services/element.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { IonRefresher, LoadingController } from '@ionic/angular';
import { PersistentConfigService } from '../services/persistent-config.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-element-detail',
  templateUrl: './element-detail.page.html',
  styleUrls: ['./element-detail.page.scss']
})
export class ElementDetailPage implements OnInit {
  element: any;
  objectId: string;
  elementSubs: Subscription;
  events: any;

  @ViewChild('refresher', { static: false }) refresher: IonRefresher;
  constructor(
    private elementService: ElementService,
    private route: ActivatedRoute,
    private persistentConfService: PersistentConfigService,
    private photoViewer: PhotoViewer
  ) {
    this.objectId = this.route.snapshot.paramMap.get('objectId');
  }

  async ngOnInit() {
    this.element = this.elementService.getElementLocal();
    this.events = this.persistentConfService.getEventsLocal();
    this.elementSubs = this.elementService
      .getElementListener()
      .subscribe(response => {
        this.element = response.element;
        this.refresher.complete();
        console.log(this.element);
      });
    // this.element = this.elementService.getElement(this.objectId);
    console.log(this.element);
  }

  showPic(ipfsURL) {
    this.photoViewer.show(environment.ipfsUrl + ipfsURL);
  }
  async doRefresh() {
    this.elementService.getElement(this.objectId);
  }
}
