import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementDetailPage } from './element-detail.page';

describe('ElementDetailPage', () => {
  let component: ElementDetailPage;
  let fixture: ComponentFixture<ElementDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
