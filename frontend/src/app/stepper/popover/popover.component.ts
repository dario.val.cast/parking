import { Component } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss']
})
export class PopoverComponent {
  event: any;
  constructor(private navParams: NavParams) {}

  ionViewWillEnter() {
    this.event = this.navParams.get('event');
  }
}
