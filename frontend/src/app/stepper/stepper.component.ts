import { Component, OnInit, Input, ViewChild, OnChanges } from '@angular/core';
import {
  PopoverController,
  NavController,
  IonContent,
  IonRefresher
} from '@ionic/angular';
import { PopoverComponent } from './popover/popover.component';
import { PopoverOptions } from '@ionic/core';
import { trigger, style, animate, transition } from '@angular/animations';
import { Subscription } from 'rxjs';
import { PersistentConfigService } from '../services/persistent-config.service';
import { ElementService } from '../services/element.service';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit, OnChanges {
  @ViewChild('ionContent', { static: false }) content: IonContent;
  @Input() element: any;
  @Input() isLoading: boolean;
  popOveroptions: PopoverOptions;
  elementSubscription: Subscription;
  @ViewChild('refresher', { static: false }) refresher: IonRefresher;
  events = [];

  constructor(
    public popoverController: PopoverController,
    private persistentConfigService: PersistentConfigService,
    private elementService: ElementService
  ) {
    this.events = this.persistentConfigService.getEventsLocal();
  }

  ngOnInit() {
    this.elementSubscription = this.elementService
      .getElementListener()
      .subscribe(element => {
        this.element = element;
        this.refresher.complete();
      });
  }

  async doRefresh() {
    this.elementService.getElement(this.element.objectId);
  }

  ngOnChanges() {
    for (let i = 0; i <= this.element.state; i++) {
      this.events[i].userId = this.element.events[i].userId;
      this.events[i].timeStamp = this.element.events[i].timeStamp;
    }
    setTimeout(() => {
      if (this.element.state >= 3) {
        this.scroll(this.element.state);
      }
    }, 500);
  }

  async presentPopover(ev: any, event: any) {
    if (event.eventType > this.element.state) {
      return;
    }
    console.log(event);
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      event: ev,
      componentProps: { event },
      translucent: true
    });
    return await popover.present();
  }

  scroll(id) {
    const yOffset = document.getElementById(id).offsetTop;
    this.content.scrollByPoint(0, yOffset, 1000);
  }
}
