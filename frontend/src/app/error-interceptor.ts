import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { Injectable } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private alertCtrl: AlertController,
    private navCntrl: NavController
  ) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        this.alertCtrl
          .create({
            header: 'Ups! something went wrong',
            message: error.error.message,
            buttons: [
              {
                text: 'Ok',
                role: 'cancel'
              }
            ]
          })
          .then(alert => {
            alert.present();
            this.navCntrl.navigateRoot('');
          });
        console.log(error.error.message);
        return throwError(error);
      })
    );
  }
}
