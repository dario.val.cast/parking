import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { PersistentConfigService } from '../services/persistent-config.service';
import { Subscription } from 'rxjs';
import { Role } from '../role.model';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.page.html',
  styleUrls: ['./create-user.page.scss']
})
export class CreateUserPage implements OnInit {
  roles: Role[] = [];
  rolesSubscription: Subscription;
  constructor(
    private authService: AuthService,
    private persistetnConfigService: PersistentConfigService
  ) {
    this.rolesSubscription = this.persistetnConfigService
      .getRolesListener()
      .subscribe(response => {
        this.roles = [...response.roles];
      });
  }

  ngOnInit() {
    this.persistetnConfigService.getRoles();
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const userRole = form.value.role;
    this.authService.createUserInvitation({ email, userRole });
  }
}
