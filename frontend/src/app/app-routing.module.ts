import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
    canLoad: [AuthGuard]
  },
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthPageModule'
  },
  {
    path: 'inspect',
    pathMatch: 'full',
    loadChildren: './inspect/inspect.module#InspectPageModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'create-element/:objectId',
    pathMatch: 'full',
    loadChildren:
      './create-element/create-element.module#CreateElementPageModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'element-status',
    pathMatch: 'full',
    loadChildren: './event-stepper/event-stepper.module#EventStepperPageModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardPageModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'create-user',
    loadChildren: './create-user/create-user.module#CreateUserPageModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'element-detail/:objectId',
    loadChildren:
      './element-detail/element-detail.module#ElementDetailPageModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'welcome',
    loadChildren: './welcome/welcome.module#WelcomePageModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'profile',
    loadChildren: './profile/profile.module#ProfilePageModule',
    canLoad: [AuthGuard]
  },
  {
    path: 'element-list',
    loadChildren: './element-list/element-list.module#ElementListPageModule',
    canLoad: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
