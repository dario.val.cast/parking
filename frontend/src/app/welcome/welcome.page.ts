import { Component, OnInit } from '@angular/core';
import { User } from '../user.model';
import { AuthService } from '../auth/auth.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss']
})
export class WelcomePage implements OnInit {
  user: User;
  logoUrl = 'assets/welcome.png';
  constructor(
    private authService: AuthService,
    public navCtrl: NavController
  ) {}

  ngOnInit() {
    this.user = this.authService.getUser();
  }
}
