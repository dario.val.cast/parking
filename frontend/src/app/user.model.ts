import { Role } from './role.model';

export interface User {
  id: string;
  role: Role;
  pub_key: any;
  push_token: any;
  first_name: string;
  last_name: string;
  isNew: boolean;
}
