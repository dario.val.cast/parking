export const environment = {
  production: true,
  // apiUrl: 'http://192.168.1.131:3005',
  // ipfsUrl: 'http://192.168.1.131:8080/ipfs/'
  apiUrl: 'http://localhost:3005',
  ipfsUrl: 'http://localhost:8080/ipfs/'
};
