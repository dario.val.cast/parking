```bash
$ ionic cordova build android --prod --release # inside /bimtrace/frontend-app

$ cp ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk ./keystoreAPK/ # inside ./bimtrace/frontend-app

$ cd ./keystoreAPK # inside ./bimtrace/frontend-app

$ jarsigner.exe -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore app-release-unsigned.apk bimtrace #password m1ns41tBIM # inside ./bimtrace/frontend-app/keystoreAPK


# (optional) Remove previous BIMTrace.apk file 'rm BIMTrace.apk'
$ zipalign -v 4 app-release-unsigned.apk BIMTrace.apk
```
