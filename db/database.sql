CREATE SCHEMA bimdocs;
CREATE SCHEMA bimtrace;

CREATE TABLE bimtrace.roles(
  id INTEGER primary key,
  display_name VARCHAR NOT NULL,
  logo VARCHAR DEFAULT 'body'
);

CREATE TABLE bimtrace.users
(
  email VARCHAR PRIMARY KEY,
  password VARCHAR NOT NULL,
  pub_key VARCHAR UNIQUE,
  push_token VARCHAR,
  user_role INTEGER NOT NULL,
  FOREIGN KEY (user_role) REFERENCES bimtrace.roles(id),
  first_name VARCHAR NOT NULL,
  last_name VARCHAR NOT NULL,
  isNew BOOLEAN DEFAULT TRUE
);


CREATE TABLE bimdocs.users
(
  email VARCHAR PRIMARY KEY,
  pass VARCHAR,
  pub_key VARCHAR UNIQUE
);

CREATE TABLE bimdocs.documents
(
  id VARCHAR PRIMARY KEY,
  owner_email VARCHAR NOT NULL,
  FOREIGN KEY (owner_email) REFERENCES bimdocs.users(email),
  mimetype VARCHAR NOT NULL,
  category VARCHAR,
  doc_state INTEGER DEFAULT 0,
  isifc BOOL DEFAULT FALSE,
  isipfs BOOL DEFAULT FALSE
);

CREATE TABLE bimdocs.versions
(
  v_hash VARCHAR PRIMARY KEY,
  document_id VARCHAR NOT NULL,
  FOREIGN KEY (document_id) REFERENCES bimdocs.documents(id),
  owner_email VARCHAR NOT NULL,
  FOREIGN KEY (owner_email) REFERENCES bimdocs.users(email),
  ifcowner VARCHAR,
  v_name VARCHAR NOT NULL,
  v_title VARCHAR NOT NULL,
  v_path VARCHAR NOT NULL,
  size INTEGER NOT NULL,
  v_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  v_signature VARCHAR DEFAULT null
);

CREATE TABLE bimtrace.searched
(
  userId VARCHAR,
  objectId VARCHAR NOT NULL, 
  FOREIGN KEY(userId) REFERENCES bimtrace.users(email),
  PRIMARY KEY(userId, objectId)
);


INSERT INTO bimdocs.users
  (email, pass)
VALUES
  ('jane@doe.com', 'password');

INSERT INTO bimdocs.documents
  (id, owner_email, mimetype, category, doc_state )
VALUES
  ('doc_hash', 'jane@doe.com', 'mimetype', 'category', 0 );

INSERT INTO bimdocs.versions
  (v_hash, document_id, owner_email, v_name, v_title, v_path, size)
VALUES('hash', 'doc_hash', 'jane@doe.com', 'v_name', 'v_title', 'v_path', 12);

insert into bimtrace.roles(id, display_name) values (0, 'BIM Manager');
-- insert into bimtrace.roles(id, display_name) values (1, 'Engineer');
-- insert into bimtrace.roles(id, display_name) values (2, 'Buyer');
insert into bimtrace.roles(id, display_name) values (3, 'Manufacturer');
insert into bimtrace.roles(id, display_name) values (4, 'Supplier');
insert into bimtrace.roles(id, display_name) values (5, 'Contractor');
insert into bimtrace.roles(id, display_name) values (6, 'Inspector');
insert into bimtrace.users
(email, password, user_role, first_name, last_name)
values
('admin@minsait.com', '$2a$10$VUmMyn7fNAwRQVoroqV0lOrD43bndRJexp8UMWsVmB1/V0HT5MlV2', 0, 'Admin', 'Minsait');

-- admin@minsait.com, m1ns41tBIM